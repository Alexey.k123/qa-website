package pages;

import base.BasePage;
import com.codeborne.selenide.SelenideElement;
import meta.PageElement;

import static com.codeborne.selenide.Selenide.$x;

//Стартовая страница
public class MainPage extends BasePage {

    public MainPage() {
        // ПРОВЕРКА - Выберите категорию
    }

    public MainPage(String selectCategory, String searchByAds) {
        // ПРОВЕРКА - элемента Выберите категорию и элемента Поиск по объявлениям
    }

    @PageElement("Выберите категорию - Недвижимость")
    public SelenideElement realEstate = $x("//div[@class='sc-bEbCYf iPJTjK']//*[text()='Недвижимость']");

    @PageElement("Выберите категорию - Легковые автомобили")
    public SelenideElement cars = $x("//div[@class='sc-bEbCYf iPJTjK']//*[text()='Легковые автомобили']");

}
